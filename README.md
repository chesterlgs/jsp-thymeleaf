# nodejs模板引擎服务器-jsp-thymeleaf

#### 项目介绍
所谓传统开发，一般就是把静态页面先写好，等后端把页面路由弄好了，再将这些静态页面改成jsp等形式放到项目中。这些“套”好的页面，脱离了环境单独打开是不能查看的。这给分离开发带来了不便。

但我们可以用nodejs模拟一个服务，提供接口和测试数据，页面模板则使用nodejs第三方插件包转换一下，返回给前端。与搭建其它服务器相比，nodejs环境配置和建服务器更快速，切换模板引擎也更方便。虽然也涉及一些后端，但nodejs使用javascript，前端付出的学习成本更低。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)