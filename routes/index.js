var express = require('express');
var router = express.Router();
var fs = require('fs');
var path = require('path');
var relativePath = '../project/templates/';
// thymeleaf
var thymeleaf  = require('thymeleaf');
var thymeleafTemplate = thymeleaf.TemplateEngine;
var StandardDialect = thymeleaf.StandardDialect;

var templateEngine = new thymeleafTemplate({
  dialects: [
    new StandardDialect('th')
  ]
});
function getThymeleaf(filePath, params, handle) {
  var html = fs.readFileSync(path.join(__dirname, relativePath,filePath), {
    encoding: 'utf-8'
  });
  html = handle(html);
  return new Promise((resolve,reject) => {
    templateEngine.process(html, params)
    .then(result => {
      resolve(result)
    });
  })
}
//jsp
const JSPJs = require('jsp-js').Renderer;
const jsp = new JSPJs({
  root: [path.join(__dirname, relativePath)]
})
function getJsp(filePath, params, handle) {
  return new Promise((resolve,reject) => {
    var html = jsp.render(filePath, params);
      html = handle(html);
      resolve(html);
  })
}

router.get('/thymeleaf', function(req, res, next) {
  getThymeleaf('home/index.html', {
    name: 'thymeleaf',
    img: '/img/default.jpg'
 }, function(html){
   html = html.replace("${type}!='manage'", "true")
   return html
  })
  .then(result => {
    res.send(result)
  });
});
router.get('/jsp', function(req, res, next) {
  getJsp('home/index.jsp', {
    name: 'jsp',
    img: '/img/default.jpg'
 }, function(html){
   html = html.replace("${type}!='manage'", "true")
   return html
  })
  .then(result => {
    res.send(result)
  });
});
module.exports = router;
